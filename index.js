const getElement = (element, classList, id) => {
  const newElement = document.createElement(element);
  if (classList)
    newElement.className = classList;
  if (id) {
    newElement.id = id;
  }
  return newElement;
}

const showError = (targetElement, message) => {
  inputError.textContent = message;
  targetElement.focus();
}

const hideError = (targetElement) => {
  targetElement.textContent = '';
}

const loadBody = () => {
  const container = getElement('div', 'container');
  document.body.appendChild(container);

  const containerLevelOne = getElement('div', 'flex-row shado padding-50');
  container.appendChild(containerLevelOne);

  const containerLevelTwo = getElement('div', 'flex-column half-width center shado padding-50');
  containerLevelOne.appendChild(containerLevelTwo);

  const headingElement = getElement('h1', 'heading-one');
  headingElement.textContent = 'Shopping';
  containerLevelTwo.appendChild(headingElement);

  const selectElement = getElement('select', '', 'user-name');

  const option1 = getElement('option');
  option1.setAttribute('selected', 'selected');
  option1.setAttribute('disabled', 'disabled');
  option1.setAttribute('value', 'none');
  option1.textContent = 'Select User';
  selectElement.appendChild(option1);

  const option2 = getElement('option');
  option2.value = 'Alice';
  option2.textContent = 'Alice';
  selectElement.appendChild(option2);

  const option3 = getElement('option');
  option3.value = 'Bob';
  option3.textContent = 'Bob';
  selectElement.appendChild(option3);

  containerLevelTwo.appendChild(selectElement);

  const inputElement = getElement('input', '', 'user-input');
  inputElement.setAttribute('type', 'text');
  containerLevelTwo.appendChild(inputElement);

  const addItemButton = getElement('button', 'add-item hide', 'add-item-button');
  addItemButton.textContent = "Add Item";
  containerLevelTwo.appendChild(addItemButton);

  const errorMessage = getElement('span', 'error-message', 'input-error');
  containerLevelTwo.appendChild(errorMessage);

  const shoppedItemsContainer = getElement('div', 'half-width shado padding-50 hide', 'shopped-items');
  containerLevelOne.appendChild(shoppedItemsContainer);

}

const shopping = () => {

  loadBody();

  const userName = document.getElementById('user-name');
  const userInput = document.getElementById('user-input');
  const inputError = document.getElementById('input-error');
  const shoppedItems = document.getElementById('shopped-items');
  const addItemButton = document.getElementById('add-item-button');

  const shoppingList = { Alice: {}, Bob: {}, itemId: 1 };

  const addItem = () => {
    const { itemId } = shoppingList;
    shoppingList[userName.value][itemId] = { itemName: userInput.value, itemStatus: 'purchase-mark', description: "Purchase" };
    shoppingList.itemId += 1;
    userInput.value = '';
  }

  const deleteItem = (elementId) => {
    const element = document.getElementById(`div-${elementId}`);
    if (element) {
      element.remove();
      delete shoppingList[userName.value][parseInt(elementId, 10)];
    }
  }

  const purchaseItem = (elementId) => {
    const element = document.getElementById(elementId);
    const itemId = elementId.split('-')[1];
    if (element.className === 'purchase-mark') {
      element.classList.replace("purchase-mark", "purchased-mark");
      element.textContent = 'Purchased';
      shoppingList[userName.value][itemId].itemStatus = 'purchased-mark';
      shoppingList[userName.value][itemId].description = 'Purchased';
    } else {
      element.classList.replace("purchased-mark", "purchase-mark");
      element.textContent = 'Purchase';
      shoppingList[userName.value][itemId].itemStatus = 'purchase-mark';
      shoppingList[userName.value][itemId].description = 'Purchase';
    }
  }

  const render = () => {
    const htmlString = { items: `<h1> ${userName.value} shopped items</h1>`, count: 1 };
    const userShoppedItems = shoppingList[userName.value];
    for (let itemId in userShoppedItems) {
      const shoppedItem = userShoppedItems[itemId];
      htmlString.items += `
      <div class="item" id = div-${itemId}>
        <p> ${htmlString.count}. ${shoppedItem.itemName}</p>
        <div>
          <button id = p-${itemId} class=${shoppedItem.itemStatus}>${shoppedItem.description}</button>
          <button id = ${itemId}>Delete</button>
        </div>
      </div>`;
      htmlString.count += 1;
    }
    if (htmlString.count === 1) {
      htmlString.items = `<h1> ${userName.value}'s cart is empty</h1>`;
    }

    shoppedItems.innerHTML = htmlString.items;
    shoppedItems.classList.remove('hide');
    shoppedItems.addEventListener('click', e => {
      if (e.target.tagName === 'BUTTON') {
        const elementId = e.target.id;
        if (elementId.charAt(0) === 'p') {
          purchaseItem(elementId);
        } else {
          deleteItem(elementId);
          render();
        }
      }
      e.stopImmediatePropagation();
    }, false);
  }

  return { userName, userInput, addItemButton, inputError, addItem, render };

}

//==================================================//

const { userName, userInput, addItemButton, inputError, addItem, render } = shopping();

userName.addEventListener('change', e => {
  if(userName.value!=='none'){
    hideError(inputError);
  }
  render();
});

userInput.addEventListener('keydown', e => {
  if (userName.value !== 'none') {
    if (e.key === 'Enter') {
      if (userInput.value !== '') {
        addItem();
        render();
      } else {
        showError(userInput, 'Please provide a valid item name');
      }
    } else {
      hideError(inputError);
    }
  } else {
    showError(userName, 'Please select one user');
  }
});

addItemButton.addEventListener('click', e => {
  if (userName.value !== 'none') {
    if (userInput.value !== '') {
      addItem();
      render();

    } else {
      showError(userInput, 'Please provide a valid item name');
    }
  } else {
    showError(userName, 'Please select one user');
  }
});

//==================================================//